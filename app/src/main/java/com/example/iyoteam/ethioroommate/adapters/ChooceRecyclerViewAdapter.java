package com.example.iyoteam.ethioroommate.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.iyoteam.ethioroommate.R;
import com.example.iyoteam.ethioroommate.activities.AddCharacterActivity;
import com.example.iyoteam.ethioroommate.activities.UserDetailActivity;
import com.example.iyoteam.ethioroommate.actors.Interest;
import com.example.iyoteam.ethioroommate.actors.Language;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mikee on 3/26/2018.
 */

public class ChooceRecyclerViewAdapter extends RecyclerView.Adapter<ChooceRecyclerViewAdapter.ViewHolder> {

    private List<Language> languages;
    private List<Interest> interests;
    private List<String> dataArray;
    private Context context;
    private int type;

    /**
     * Flag 0 for languages
     * Flag -1 for dislikes
     * Flag 1 for likes
     *
     * @param interests
     * @param context
     */
    public ChooceRecyclerViewAdapter(Context context, List<Interest> interests, int type) {
        this.interests = interests;
        this.context = context;
        dataArray = new ArrayList<>();

        for (Interest interest : interests) {
            dataArray.add(interest.getName());
        }
        this.type = type;
    }

    public ChooceRecyclerViewAdapter(Context context, List<Language> _languages) {
        this.languages = new ArrayList<>();
        languages.addAll(_languages);
        this.context = context;
        this.dataArray = new ArrayList<>();

        for (Language language : languages) {
            dataArray.add(language.getName());
        }

        this.type = AddCharacterActivity.TYPE_LANGUAGES;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.interest_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final String text = dataArray.get(position);
        holder.itemTextView.setText(text);

        holder.removeItemImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (type) {
                    case AddCharacterActivity.TYPE_LANGUAGES:
                        Language _language = languages.get(position);
                        ((AddCharacterActivity) context).removeLanguage(_language);
                        break;
                    case AddCharacterActivity.TYPE_DISLIKES:
                        ((AddCharacterActivity) context).removeDislike(position);
                        break;
                    case AddCharacterActivity.TYPE_LIKES:
                        ((AddCharacterActivity) context).removeLike(position);
                        break;
                    default:
                        break;
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return dataArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView itemTextView;
        public ImageButton removeItemImageButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemTextView = itemView.findViewById(R.id.interestTextViewOnItem);
            removeItemImageButton = itemView.findViewById(R.id.removeInterestImageBtnOnItem);
        }
    }

}
