package com.example.iyoteam.ethioroommate.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.iyoteam.ethioroommate.R;

public class ChooceActivity extends AppCompatActivity {

    private Button registerBtn;
    private Button uploadBtn;
    private Button houseListBtn;
    private Button roommateListBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chooce);

        registerBtn = findViewById(R.id.chooseRegisterBtn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(ChooceActivity.this, SignUpActivity.class);
                startActivity(registerIntent);
            }
        });

        uploadBtn = findViewById(R.id.chooseUploadHouseBtn);
        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent uploadIntent = new Intent(ChooceActivity.this, UploadRoomActivity.class);
                startActivity(uploadIntent);
            }
        });

        houseListBtn = findViewById(R.id.chooseHouseListBtn);
        houseListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent houseListIntent =  new Intent(ChooceActivity.this, RoomListActivity.class);
                startActivity(houseListIntent);
            }
        });

        roommateListBtn = findViewById(R.id.chooseRoommateListBtn);
        roommateListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent  userListActivity = new Intent(ChooceActivity.this, UserListActivity.class);
                startActivity(userListActivity);
            }
        });

    }
}
