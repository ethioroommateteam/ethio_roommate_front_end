package com.example.iyoteam.ethioroommate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.iyoteam.ethioroommate.R;
import com.example.iyoteam.ethioroommate.actors.Characteristic;
import com.example.iyoteam.ethioroommate.actors.Interest;
import com.example.iyoteam.ethioroommate.actors.Language;
import com.example.iyoteam.ethioroommate.actors.SimpleObject;
import com.example.iyoteam.ethioroommate.actors.User;
import com.example.iyoteam.ethioroommate.adapters.SimpleRecyclerViewAdapter;

import java.util.List;

public class UserDetailActivity extends AppCompatActivity {

    private RecyclerView languageRecyclerView;
    private RecyclerView interestsRecyclerView;
    private RecyclerView dislikesRecyclerView;
    private TextView currentJobTextView;
    private TextView monthlyIncomeTextView;
    private TextView religionTextView;
    private TextView sexTextView;
    private TextView ageTextView;

    private List<Language> languages;
    private List<Interest> likes;
    private List<Interest> dislikes;
    private User user;

    private SimpleRecyclerViewAdapter languageRecyclerViewAdapter;
    private SimpleRecyclerViewAdapter interestsRecyclerViewAdapter;
    private SimpleRecyclerViewAdapter dislikesRecyclerViewAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        user = (User) intent.getSerializableExtra("user");

        setContentView(R.layout.upload_room);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("@"+user.getUserName());

        initializeItems();
        populateElementsWithUserValue();
    }

    private void initializeItems(){
        languageRecyclerView = findViewById(R.id.languageRecyclerView);
        interestsRecyclerView = findViewById(R.id.interestsRecyclerView);
        dislikesRecyclerView = findViewById(R.id.dislikesRecyclerView);
        currentJobTextView = findViewById(R.id.currentJobTextView);
        monthlyIncomeTextView = findViewById(R.id.monthlyIncomeTextView);
        religionTextView = findViewById(R.id.religionTextView);
        ageTextView = findViewById(R.id.ageTextViewOnUserDetail);
        sexTextView = findViewById(R.id.sexTextViewOnUserDetail);
    }

    private void populateElementsWithUserValue(){

        Characteristic userCharacteristic = user.getCharacteristic();
        //Populate language
        languages = userCharacteristic.getLanguages();


        languageRecyclerViewAdapter = new SimpleRecyclerViewAdapter(languages, this);
        languageRecyclerView.setHasFixedSize(true);
        languageRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        languageRecyclerView.setAdapter(languageRecyclerViewAdapter);


        //Populate interests
        likes = userCharacteristic.getLikes();
        interestsRecyclerViewAdapter = new SimpleRecyclerViewAdapter(likes, this);
        interestsRecyclerView.setHasFixedSize(true);
        interestsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        interestsRecyclerView.setAdapter(interestsRecyclerViewAdapter);

        //Populate dislikes
        dislikes = userCharacteristic.getDislikes();
        dislikesRecyclerViewAdapter = new SimpleRecyclerViewAdapter(dislikes, this);
        dislikesRecyclerView.setHasFixedSize(true);
        dislikesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        dislikesRecyclerView.setAdapter(dislikesRecyclerViewAdapter);

        currentJobTextView.setText(userCharacteristic.getJob());
        monthlyIncomeTextView.setText(userCharacteristic.getIncome().toString() + " Birr monthly income");

        ageTextView.setText(user.getAge().toString());
        sexTextView.setText(user.getSexName());
        religionTextView.setText(userCharacteristic.getReligion());

    }


}
