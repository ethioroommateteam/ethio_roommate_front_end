package com.example.iyoteam.ethioroommate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.iyoteam.ethioroommate.R;

public class TekerayActivity extends AppCompatActivity {


    private Button submitBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tekeray_profile);

        submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent uploadRentActivity = new Intent(TekerayActivity.this, UploadRoomActivity.class);
                startActivity(uploadRentActivity);
            }
        });
    }

}
