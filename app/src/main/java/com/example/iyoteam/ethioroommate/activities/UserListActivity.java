package com.example.iyoteam.ethioroommate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.FrameLayout;

import com.example.iyoteam.ethioroommate.actors.Characteristic;
import com.example.iyoteam.ethioroommate.actors.User;
import com.example.iyoteam.ethioroommate.dbhandlers.UserDBHandler;
import com.example.iyoteam.ethioroommate.R;
import com.example.iyoteam.ethioroommate.adapters.UserRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class UserListActivity extends AppCompatActivity {

    private UserRecyclerViewAdapter userRecyclerViewAdapter;
    private RecyclerView recyclerView;
    private FrameLayout frameLayout;
    private UserDBHandler userDBHandler;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_list_layout);

        recyclerView = findViewById(R.id.userListRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        userDBHandler = new UserDBHandler(this);
        userDBHandler.getUserList();
    }

    public void showUsers(List<User> users){
        userRecyclerViewAdapter = new UserRecyclerViewAdapter(users, this);
        recyclerView.setAdapter(userRecyclerViewAdapter);
    }

    public void showUserDetail(User user){
        if(user.getCharacteristic().getJob().length() <= 0){
            Intent intent = new Intent(UserListActivity.this, AddCharacterActivity.class);
            intent.putExtra("user", user);
            startActivity(intent);
        }

        Intent intent = new Intent(UserListActivity.this, UserDetailActivity.class);
        intent.putExtra("user", user);
        startActivity(intent);
    }

    public void getUserCharacter(User user){
/**        Intent intent = new Intent(UserListActivity.this, AddCharacterActivity.class);
//        intent.putExtra("user", user);
//        startActivity(intent);  */
        userDBHandler.getUserCharacter(user);
    }


}
