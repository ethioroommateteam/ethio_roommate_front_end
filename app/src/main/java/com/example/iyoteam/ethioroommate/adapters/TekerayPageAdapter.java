package com.example.iyoteam.ethioroommate.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.iyoteam.ethioroommate.R;
import com.example.iyoteam.ethioroommate.fragments.AllFragment;
import com.example.iyoteam.ethioroommate.fragments.HouseWithRoommateFragment;
import com.example.iyoteam.ethioroommate.fragments.NewRoommateslFragment;

/**
 * Created by Mikee on 3/25/2018.
 */

public class TekerayPageAdapter extends FragmentPagerAdapter {

    private Context context;

    public TekerayPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new AllFragment();
            case 1:
                return new HouseWithRoommateFragment();
            case 2:
                return new NewRoommateslFragment();
            default:
                break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        String tekeray_tab_names[] = context.getResources().getStringArray(R.array.tekeray_tab_names);
        return super.getPageTitle(position);
    }
}
