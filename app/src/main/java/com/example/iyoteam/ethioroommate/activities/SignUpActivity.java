package com.example.iyoteam.ethioroommate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.iyoteam.ethioroommate.actors.User;
import com.example.iyoteam.ethioroommate.dbhandlers.UserDBHandler;
import com.example.iyoteam.ethioroommate.R;

public class SignUpActivity extends AppCompatActivity {

    private UserDBHandler userDBHandler;
    private LinearLayout signUpNamesLayout;
    private LinearLayout signUpPasswordLayout;
    private LinearLayout chooceActionLayout;

    private ImageButton namesNextBtn;
    private ImageButton passwordPrevBtn;
    private ImageButton passwordNextBtn;

    private EditText fnameEditText;
    private EditText lnameEditText;
    private EditText userNameEditText;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;
    private EditText phoneNumberEditText;
    private EditText ageEditText;
    private RadioGroup sexTypeRadioGroup;

    private Button chooseTekerayWithHouseBtn;
    private Integer sexType;


    String fname;
    String lname;
    String userName;
    String password;
    String phoneNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_layout);

        signUpNamesLayout = findViewById(R.id.signUpNamesLayout);
        signUpPasswordLayout = findViewById(R.id.signUpPasswordLayout);
        chooceActionLayout = findViewById(R.id.chooceActionLayout);

        fnameEditText = findViewById(R.id.fnameEditText);
        lnameEditText = findViewById(R.id.lnameEditText);
        phoneNumberEditText = findViewById(R.id.phoneNmberEditText);
        ageEditText = findViewById(R.id.ageEditTextOnSignUp);
        sexTypeRadioGroup = findViewById(R.id.newUserFormSexValueRadioGroup);

        namesNextBtn = findViewById(R.id.namesNextBtn);


        namesNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fname = fnameEditText.getText().toString();
                lname = lnameEditText.getText().toString();
                phoneNumber = phoneNumberEditText.getText().toString();
                String selectedRadioBtn =((RadioButton)findViewById(sexTypeRadioGroup.getCheckedRadioButtonId())).getText().toString();
                final Integer age = Integer.parseInt(ageEditText.getText().toString());
                switch (selectedRadioBtn){
                    case "Female":
                        sexType = 0;
                        break;
                    case "Male":
                        sexType = 1;
                        break;
                    default:
                        break;
                }

                if((fname.length() >= 1) && (lname.length() >= 1) && (phoneNumber.length() >= 1)){
                    signUpNamesLayout.setVisibility(View.GONE);
                    signUpPasswordLayout.setVisibility(View.VISIBLE);

                    passwordPrevBtn = findViewById(R.id.passwordPrevBtn);
                    passwordNextBtn = findViewById(R.id.passwordNextBtn);

                    passwordPrevBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            signUpNamesLayout.setVisibility(View.VISIBLE);
                            signUpPasswordLayout.setVisibility(View.GONE);
                        }
                    });

                    passwordNextBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            userNameEditText = findViewById(R.id.userNameEditText);
                            passwordEditText = findViewById(R.id.passwordEditText);
                            confirmPasswordEditText = findViewById(R.id.confirmPasswordEditText);

                            userName = userNameEditText.getText().toString();
                            password = passwordEditText.getText().toString();
                            String confirmPassword = confirmPasswordEditText.getText().toString();


                            if( (userName.length() >= 0) && (password.length() >= 1) && (confirmPassword.length() >= 1) ){
                                if(!password.equals(confirmPassword)){
                                    Toast.makeText(SignUpActivity.this, "Password mismatch", Toast.LENGTH_LONG).show();
                                    return;
                                }

                                User user = new User(fname, lname, userName, phoneNumber, password, age, sexType);

                                userDBHandler = new UserDBHandler(SignUpActivity.this);
                                userDBHandler.insertUser(user);


                            }

                            else {
                                Toast.makeText(SignUpActivity.this, "Please enter the required fields", Toast.LENGTH_LONG).show();
                            }

                        }
                    });

                }

                else{
                    Toast.makeText(SignUpActivity.this, "Please fill all fields", Toast.LENGTH_LONG).show();
                }


            }
        });

    }

    public void clearForm(){
       Intent intent = new Intent(SignUpActivity.this, UserListActivity.class);
       startActivity(intent);
    }
}
