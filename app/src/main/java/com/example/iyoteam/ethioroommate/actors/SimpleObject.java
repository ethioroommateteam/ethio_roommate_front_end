package com.example.iyoteam.ethioroommate.actors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by user on 4/13/2018.
 */

@SuppressWarnings("serial")
public class SimpleObject implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    public SimpleObject(){};

    public SimpleObject(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public SimpleObject(String name)
    {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
