package com.example.iyoteam.ethioroommate.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.iyoteam.ethioroommate.R;
import com.example.iyoteam.ethioroommate.activities.UserDetailActivity;
import com.example.iyoteam.ethioroommate.actors.Interest;
import com.example.iyoteam.ethioroommate.actors.Language;
import com.example.iyoteam.ethioroommate.actors.SimpleObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mikee on 3/26/2018.
 */

public class SimpleRecyclerViewAdapter extends RecyclerView.Adapter<SimpleRecyclerViewAdapter.ViewHolder> {

    private List<Language> languages;
    private List<Interest> interests;
    private List<String> dataArray;


    private Context context;

    public SimpleRecyclerViewAdapter(List<Interest> interests, Context context){
        this.interests = interests;
        this.context = context;
        dataArray = new ArrayList<>();
        for (Interest interest:interests) {
            dataArray.add(interest.getName());
        }
    }

    public SimpleRecyclerViewAdapter(List<Language> languages, UserDetailActivity context) {
        this.languages = languages;
        this.context = context;
        dataArray = new ArrayList<>();
        if(languages != null){
            for (Language language: languages){
                dataArray.add(language.getName());
            }
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.languages_list_item, parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final String text = dataArray.get(position);
        holder.languageItemTextView.setText(text);
    }

    @Override
    public int getItemCount() {
        return dataArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView languageItemTextView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            languageItemTextView = itemView.findViewById(R.id.languagesItemTextView);
        }
    }

}
