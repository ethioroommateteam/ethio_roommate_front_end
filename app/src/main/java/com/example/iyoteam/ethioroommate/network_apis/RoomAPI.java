package com.example.iyoteam.ethioroommate.network_apis;

import com.example.iyoteam.ethioroommate.actors.Room;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Mikee on 3/24/2018.
 */

public interface RoomAPI {

//    For inserting new Room for rent
    @FormUrlEncoded
    @POST("eroom/public/newHouse")
    Call<Void> insertHouse(
            @Field("user_id") Integer user_id,
            @Field("location") String location,
            @Field("latitude") Double latitude,
            @Field("longitude") Double longitude,
            @Field("numberOfRooms") Integer numberOfRooms,
            @Field("numberOfPeoples") Integer numberOfPeoples,
            @Field("numberOfRoommates") Integer numberOfRoommates,
            @Field("minAge") Integer minAge,
            @Field("maxAge") Integer maxAge,
            @Field("sexType") Integer sexType,
            @Field("price") Double price,
            @Field("caption") String caption,
            @Field("description") String description);

    //    For getting house list
    @GET("eroom/public/houses")
    Call<List<Room>> getHouses(); //

}

