package com.example.iyoteam.ethioroommate.dbhandlers;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import com.example.iyoteam.ethioroommate.activities.RoomListActivity;
import com.example.iyoteam.ethioroommate.activities.UploadRoomActivity;
import com.example.iyoteam.ethioroommate.actors.Room;
import com.example.iyoteam.ethioroommate.network_apis.RoomAPI;
import com.example.iyoteam.ethioroommate.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by iyob on 3/25/2018.
 */

public class RoomDBHandler {

    private final String BASE_URL = "http://192.168.43.42";
    Context context;

    private Gson gson;
    private Retrofit retrofit;
    RoomAPI roomAPI;


    /**
     *
     * Establish a connection to server using RoomAPI class
     * */
    public RoomDBHandler(Context context) {

        this.context = context;
        try {
            gson = new GsonBuilder().setLenient().create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            roomAPI = retrofit.create(RoomAPI.class);

        } catch (Exception ex) {
            Toast.makeText(context, "Error connecting to server please try again!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * accepts room object
     * uploads the room through insertHouse method on roomAPI
     * show succesfull information on success
     * redirect to room list
     * Show error if it fails
     * @param room
     */

    public void uploadRoom(Room room) {

        //Creating object for our interface
        Call<Void> request = roomAPI.insertHouse(
                1,
                room.getLocation(),
                room.getLatitude(),
                room.getLongitude(),
                room.getNumberOfRooms(),
                room.getNumberOfPeople(),
                room.getNumberOfRoommates(),
                room.getMinAge(),
                room.getMaxAge(),
                room.getSexType(),
                room.getPrice(),
                room.getCaption(),
                room.getDescription()
        );


        request.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                Toast.makeText(context, "Succesfully added!", Toast.LENGTH_LONG).show();
                ((UploadRoomActivity)context).exitAfterUpload();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Snackbar.make(((Activity) context).findViewById(R.id.wrapperFramework), "Not Succesfull!", Snackbar.LENGTH_LONG).show();
            }

        });

    }

    /**
     *
     * Get available rooms from server
     * populate recyclerView with rhe rooms
     */

    public void getRoomList() {

        Call<List<Room>> request = roomAPI.getHouses();

        request.enqueue(new Callback<List<Room>>() {

            @Override
            public void onResponse(Call<List<Room>> call, Response<List<Room>> response) {

                if (response.isSuccessful()) {
                    ((RoomListActivity) context).showAvailableRooms(response.body());
                }

            }

            @Override
            public void onFailure(Call<List<Room>> call, Throwable throwable) {
                Snackbar.make(((Activity) context).findViewById(R.id.uploadListWrapper), "Something went wrong", Snackbar.LENGTH_LONG).show();
            }

        });


    }

}
