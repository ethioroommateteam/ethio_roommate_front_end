package com.example.iyoteam.ethioroommate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.iyoteam.ethioroommate.actors.Room;
import com.example.iyoteam.ethioroommate.dbhandlers.RoomDBHandler;
import com.example.iyoteam.ethioroommate.R;

public class UploadRoomActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FrameLayout wrapperFramework;

    //Layouts
    private LinearLayout locationsFormWrapper;
    private LinearLayout roomStatusFormWrapper;
    private LinearLayout roommateRequirementFormWrapper;
    private LinearLayout detailsFormWrapper;

    //Next Buttons
    private Button nextBtnOnLocationForm;
    private Button nextBtnOnRoomStatusForm;
    private Button nextBtnOnRoommateRequirementForm;

    //Prev Buttons
    private Button prevBtnOnRoomStatusForm;
    private Button prevBtnOnRoommateRequirementForm;
    private Button prevBtnOnDetailsForm;

    private Button uploadNewHouseBtn;


    //EditTexts for location form
    private EditText newHouseFormLocationEditText;
    private EditText newHouseFormLatitudeValueEditText;
    private EditText newHouseFormLongitudeValueEditText;

    //EditText's for room detail form
    private EditText newHouseFormNumberOfRoomsEditText;
    private EditText newHouseFormNumberOfPeopleEditText;

    //EditText's for roommate requirements form
    private EditText newHouseFormNumberOfRoommatesEditText;
    private EditText newHouseFormRentValueEditText;
    private EditText newHouseFormMinAgeValueEditText;
    private EditText newHouseFormMaxAgeValuesEditText;
    private RadioGroup newHouseFormSexChoiceRadioGroup;

    //For final
    private EditText newHouseFormCaptionEditText;
    private EditText newHouseFormDetailEditText;


    private String locationValue;
    private Double longitudeValue;
    private Double latitudeValue;

    private Integer numberOfRooms;
    private Integer numberOfPeoples;

    private Integer numberOfRoommates;
    private Double rentValue;
    private Integer minAgeValue;
    private Integer maxAgeValue;
    private Integer sexType;

    private String caption;
    private String detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roommate);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initializeComponents();
        handleButtonsOnclickListener();
    }

    private void initializeComponents() {

        wrapperFramework = findViewById(R.id.wrapperFramework);

        locationsFormWrapper = findViewById(R.id.locationsFormWrapper);
        roomStatusFormWrapper = findViewById(R.id.roomStatusFormWrapper);
        roommateRequirementFormWrapper = findViewById(R.id.roommateRequirementFormWrapper);
        detailsFormWrapper = findViewById(R.id.detailsFormWrapper);

        nextBtnOnLocationForm = findViewById(R.id.nextBtnOnLocationForm);
        nextBtnOnRoomStatusForm = findViewById(R.id.nextBtnOnRoomStatusForm);
        nextBtnOnRoommateRequirementForm = findViewById(R.id.nextBtnOnRoommateRequirementForm);

        prevBtnOnRoomStatusForm = findViewById(R.id.prevBtnOnRoomStatusForm);
        prevBtnOnRoommateRequirementForm = findViewById(R.id.prevBtnOnRoommateRequirementForm);
        prevBtnOnDetailsForm = findViewById(R.id.prevBtnOnDetailsForm);

        uploadNewHouseBtn = findViewById(R.id.uploadNewHouseBtn);

        //Initialize edittext
        //Location form
        newHouseFormLocationEditText = findViewById(R.id.newHouseFormLocationEditText);
        newHouseFormLatitudeValueEditText = findViewById(R.id.newHouseFormLatitudeValueEditText);
        newHouseFormLongitudeValueEditText = findViewById(R.id.newHouseFormLongitudeValueEditText);

        // Room Information form
        newHouseFormNumberOfRoomsEditText = findViewById(R.id.newHouseFormNumberOfRoomsEditText);
        newHouseFormNumberOfPeopleEditText = findViewById(R.id.newHouseFormNumberOfPeopleEditText);

        //Roommate reqiurement form
        newHouseFormNumberOfRoommatesEditText = findViewById(R.id.newHouseFormNumberOfRoommateEditText);
        newHouseFormRentValueEditText = findViewById(R.id.newHouseFormRentValueEditText);
        newHouseFormMinAgeValueEditText = findViewById(R.id.newHouseFormMinAgeValueEditText);
        newHouseFormMaxAgeValuesEditText = findViewById(R.id.newHouseFormMaxAgeValueEditText);
        newHouseFormSexChoiceRadioGroup = findViewById(R.id.newHouseFormSexValueRadioGroup);

//        Final form
        newHouseFormCaptionEditText = findViewById(R.id.newHouseFormCaptionEditText);
        newHouseFormDetailEditText = findViewById(R.id.newHouseFormDetailEditText);
    }

    public void handleButtonsOnclickListener() {

        nextBtnOnLocationForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check if all required values are filled
                if ((newHouseFormLocationEditText.getText().length() == 0) || (newHouseFormLatitudeValueEditText.getText().length() == 0) || (newHouseFormLongitudeValueEditText.getText().length() == 0)) {
                    Snackbar.make(wrapperFramework, "Enter required fields", Snackbar.LENGTH_LONG).show();
                    return;
                }

                locationValue = newHouseFormLocationEditText.getText().toString();
                latitudeValue = Double.parseDouble(newHouseFormLatitudeValueEditText.getText().toString());
                longitudeValue = Double.parseDouble(newHouseFormLongitudeValueEditText.getText().toString());

                locationsFormWrapper.setVisibility(View.GONE);
                roomStatusFormWrapper.setVisibility(View.VISIBLE);
            }
        });

        nextBtnOnRoomStatusForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ((newHouseFormNumberOfRoomsEditText.getText().length() == 0) || (newHouseFormNumberOfPeopleEditText.getText().length() == 0)) {
                    Snackbar.make(wrapperFramework, R.string.enter_required_field, Snackbar.LENGTH_LONG).show();
                    return;
                }

                numberOfRooms = Integer.parseInt(newHouseFormNumberOfRoomsEditText.getText().toString());
                numberOfPeoples = Integer.parseInt(newHouseFormNumberOfPeopleEditText.getText().toString());

                if(numberOfRooms <= 0){
                    Snackbar.make(wrapperFramework, R.string.room_number_less_than_zero, Snackbar.LENGTH_LONG).show();
                    return;
                }
                if(numberOfPeoples <= 0){
                    Snackbar.make(wrapperFramework, R.string.people_number_less_than_zero, Snackbar.LENGTH_LONG).show();
                    return;
                }
                roomStatusFormWrapper.setVisibility(View.GONE);
                roommateRequirementFormWrapper.setVisibility(View.VISIBLE);
            }
        });

        prevBtnOnRoomStatusForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roomStatusFormWrapper.setVisibility(View.GONE);
                locationsFormWrapper.setVisibility(View.VISIBLE);
            }
        });

        nextBtnOnRoommateRequirementForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( (newHouseFormNumberOfRoommatesEditText.getText().length() == 0) || (newHouseFormRentValueEditText.getText().length() == 0) || (newHouseFormMinAgeValueEditText.getText().length() == 0) || (newHouseFormMaxAgeValuesEditText.getText().length() == 0) || (newHouseFormSexChoiceRadioGroup.getCheckedRadioButtonId() == -1) ){
                    Snackbar.make(wrapperFramework, R.string.enter_required_field, Snackbar.LENGTH_LONG).show();
                    return;
                }


                numberOfRoommates = Integer.parseInt(newHouseFormNumberOfRoommatesEditText.getText().toString());
                rentValue = Double.parseDouble(newHouseFormRentValueEditText.getText().toString());
                minAgeValue = Integer.parseInt(newHouseFormMinAgeValueEditText.getText().toString());
                maxAgeValue = Integer.parseInt(newHouseFormMinAgeValueEditText.getText().toString());
                String selectedRadioBtn =((RadioButton)findViewById(newHouseFormSexChoiceRadioGroup.getCheckedRadioButtonId())).getText().toString();

                switch (selectedRadioBtn){
                    case "Both":
                        sexType = -1;
                        break;
                    case "Male":
                        sexType = 0;
                        break;
                    case "Female":
                        sexType = 1;
                        break;
                    default:
                        break;
                }

                if(numberOfRoommates < 1){
                    Snackbar.make(wrapperFramework, R.string.reasonable_roommate_number_caution, Snackbar.LENGTH_LONG).show();
                    return;
                }

                if(rentValue <= 100){
                    Snackbar.make(wrapperFramework, R.string.reasonable_rent_value_caution, Snackbar.LENGTH_LONG).show();
                    return;
                }

                if(minAgeValue < 18){
                    Snackbar.make(wrapperFramework, R.string.reasonable_min_age_caution, Snackbar.LENGTH_LONG).show();
                    return;
                }

                if(maxAgeValue > 80){
                    Snackbar.make(wrapperFramework, R.string.reasonable_max_age_caution, Snackbar.LENGTH_LONG).show();
                    return;
                }

                if(minAgeValue > maxAgeValue){
                    Snackbar.make(wrapperFramework, R.string.reasonable_age_range_value_caution, Snackbar.LENGTH_LONG).show();
                    return;
                }

                roommateRequirementFormWrapper.setVisibility(View.GONE);
                detailsFormWrapper.setVisibility(View.VISIBLE);
            }
        });

        prevBtnOnRoommateRequirementForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roommateRequirementFormWrapper.setVisibility(View.GONE);
                roomStatusFormWrapper.setVisibility(View.VISIBLE);
            }
        });

        uploadNewHouseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((newHouseFormCaptionEditText.getText().length() == 0) || (newHouseFormDetailEditText.getText().length() == 0)) {
                    Snackbar.make(wrapperFramework, R.string.enter_required_field, Snackbar.LENGTH_LONG).show();
                    return;
                }
                caption = newHouseFormCaptionEditText.getText().toString();
                detail = newHouseFormDetailEditText.getText().toString();
                Room room = new Room(locationValue, latitudeValue, longitudeValue, numberOfRooms, numberOfPeoples, numberOfRoommates, rentValue, minAgeValue, maxAgeValue, sexType, caption, detail);
                RoomDBHandler roomdbHandler = new RoomDBHandler(UploadRoomActivity.this);
                roomdbHandler.uploadRoom(room);

            }
        });

        prevBtnOnDetailsForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detailsFormWrapper.setVisibility(View.GONE);
                roommateRequirementFormWrapper.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.roommate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void exitAfterUpload(){
        Intent intent = new Intent(UploadRoomActivity.this, RoomListActivity.class);
        startActivity(intent);
    }
}
