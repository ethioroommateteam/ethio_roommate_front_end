package com.example.iyoteam.ethioroommate.network_apis;

import com.example.iyoteam.ethioroommate.actors.Characteristic;
import com.example.iyoteam.ethioroommate.actors.Interest;
import com.example.iyoteam.ethioroommate.actors.Language;
import com.example.iyoteam.ethioroommate.actors.Religion;
import com.example.iyoteam.ethioroommate.actors.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Mikee on 3/24/2018.
 */

public interface UserAPI {

    @FormUrlEncoded
    @POST("eroom/public/newUser")
    Call<Void> insertUser(
            @Field("fname") String fame,
            @Field("lname") String lname,
            @Field("user_name") String user_name,
            @Field("phone_number") String phone_number,
            @Field("password") String password,
            @Field("age") Integer age,
            @Field("sex") Integer sex);
//            Callback<Response> callback);

    // For getting user list
    @GET("eroom/public/users")
    Call<List<User>> getUsers();

    // For getting religion list
    @GET("eroom/public/religions")
    Call<List<Religion>> getReligions();

    // For getting language list
    @GET("eroom/public/languages")
    Call<List<Language>> getLanguages();

    // For getting interest list
    @GET("eroom/public/interests")
    Call<List<Interest>> getInterests();

    // For getting user list
    @GET("eroom/public/crx/{id}")
    Call<Characteristic> getUserCharacter(@Path("id") Integer user_id);

    @POST("eroom/public/userCrx")
    Call<User> addUserCharacter(
            @Body Characteristic characteristic
    );

}

