package com.example.iyoteam.ethioroommate.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.iyoteam.ethioroommate.R;
import com.example.iyoteam.ethioroommate.actors.Characteristic;
import com.example.iyoteam.ethioroommate.actors.Interest;
import com.example.iyoteam.ethioroommate.actors.Language;
import com.example.iyoteam.ethioroommate.actors.Religion;
import com.example.iyoteam.ethioroommate.actors.User;
import com.example.iyoteam.ethioroommate.adapters.ChooceRecyclerViewAdapter;
import com.example.iyoteam.ethioroommate.dbhandlers.UserDBHandler;

import java.util.ArrayList;
import java.util.List;

public class AddCharacterActivity extends AppCompatActivity {

    public CoordinatorLayout addCharacterWrapper;
    private Button chooseReligionBtn;
    private LinearLayout religionWrapper;
    private TextView religionTextView;
    private Button changeReligionBtn;
    private Button addLanguagesBtn;
    private Button addLikesBtn;
    private Button addDisLikesBtn;

    private ImageButton submitCharacter;
    private EditText jobEditText;
    private EditText incomeEditText;
    private EditText detailEditText;

    private List<Religion> religions;
    private Religion selectedReligion;
    private List<Interest> interests;
    private List<Interest> likes;
    private List<Interest> dislikes;
    private List<Language> languages;
    private List<Language> selectedLanguages;

    private RecyclerView languagesRecyclerView;
    private RecyclerView likesRecyclerView;
    private RecyclerView dislikesRecyclerView;

    private ChooceRecyclerViewAdapter likesAdapter;
    private ChooceRecyclerViewAdapter dislikesAdapter;
    private ChooceRecyclerViewAdapter languagesAdapter;
    private User user;

    public final static int TYPE_LIKES = 1;
    public final static int TYPE_DISLIKES = -1;
    public final static int TYPE_LANGUAGES = 0;

    private UserDBHandler userDBHandler;
    private int interest_type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Intent intent = getIntent();
//        user = (User) intent.getSerializableExtra("user");

        setContentView(R.layout.add_character);
        Toolbar toolbar = findViewById(R.id.toolbar_on_add_character);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("@"+user.getUserName());

        initializeItems();
    }

    private void initializeItems() {
        addCharacterWrapper = findViewById(R.id.coordinateLayoutOnAddCharacter);
        chooseReligionBtn = findViewById(R.id.chooseReligionBtnOnAddCharacter);
        religionWrapper = findViewById(R.id.religionWrapperLinearLayout);
        religionTextView = findViewById(R.id.religionTextViewOnAddCharacter);
        changeReligionBtn = findViewById(R.id.changeReligionBtnOnAddCharacter);
        addLanguagesBtn = findViewById(R.id.addLanguageBtnOnAddCharacter);
        addLikesBtn = findViewById(R.id.addLikesBtnOnAddCharacter);
        addDisLikesBtn = findViewById(R.id.addDiLikesBtnOnAddCharacter);

        submitCharacter = findViewById(R.id.submitCharacterImageButtonOnAddCharacter);
        jobEditText = findViewById(R.id.jobEditTextOnAddCharacter);
        incomeEditText = findViewById(R.id.incomeEditTextOnAddCharacter);
        detailEditText = findViewById(R.id.detailEditTextOnAddCharacter);

        languagesRecyclerView = findViewById(R.id.languagesRecyclerViewOnAddCharacter);
        likesRecyclerView = findViewById(R.id.likesRecyclerViewOnAddCharacter);
        dislikesRecyclerView = findViewById(R.id.dislikesRecyclerViewOnAddCharacter);

        selectedReligion = new Religion();
        selectedLanguages = new ArrayList<>();
        likes = new ArrayList<>();
        dislikes = new ArrayList<>();

        likesAdapter = new ChooceRecyclerViewAdapter(AddCharacterActivity.this, likes, TYPE_LIKES);
        dislikesAdapter = new ChooceRecyclerViewAdapter(AddCharacterActivity.this, dislikes, TYPE_DISLIKES);
        languagesAdapter = new ChooceRecyclerViewAdapter(AddCharacterActivity.this, selectedLanguages);

        likesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        likesRecyclerView.setHasFixedSize(false);
        likesRecyclerView.setAdapter(likesAdapter);

        dislikesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        dislikesRecyclerView.setHasFixedSize(false);
        dislikesRecyclerView.setAdapter(dislikesAdapter);

        languagesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        languagesRecyclerView.setHasFixedSize(false);
        languagesRecyclerView.setAdapter(languagesAdapter);

        userDBHandler = new UserDBHandler(AddCharacterActivity.this);

        chooseReligionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchReligionsFromDB();
            }
        });

        changeReligionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReligionDialog(true);
            }
        });

        addLanguagesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (languages == null)
                    fetchLanguagesFromDB();
                else
                    showLanguagesDialog();
            }
        });

        addLikesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interest_type = TYPE_LIKES;
                if (interests == null)
                    fetchInterestsFromDB();
                else
                    showInterestsDialog(interest_type);
            }
        });

        addDisLikesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interest_type = TYPE_DISLIKES;
                if (interests == null)
                    fetchInterestsFromDB();
                else
                    showInterestsDialog(interest_type);
            }
        });

        submitCharacter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userDBHandler.addUserCharacterisitic(6, getUserCharacteristics());
            }
        });
    }

    public void showReligionDialog(final Boolean isSelected) {

        if (religions == null) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(AddCharacterActivity.this);
        builder.setTitle(R.string.choose_religion);

        final ArrayAdapter<Religion> arrayAdapter = new ArrayAdapter<>(AddCharacterActivity.this, android.R.layout.select_dialog_item);
        arrayAdapter.addAll(religions);

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!isSelected) {
                    chooseReligionBtn.setVisibility(View.GONE);
                    religionWrapper.setVisibility(View.VISIBLE);
                }
                Religion selectedItem = arrayAdapter.getItem(i);
                religionTextView.setText(selectedItem.getName());
                selectedReligion = selectedItem;
            }
        });

        builder.show();
    }

    public void showInterestsDialog(final int type) {

        final List<Interest> selectedInterests = new ArrayList<>();

        final AlertDialog.Builder interestBuilder = new AlertDialog.Builder(AddCharacterActivity.this);
        String title = "";
        switch (type) {
            case TYPE_DISLIKES:
                title = getResources().getString(R.string.choose_dislikes);
                break;
            case TYPE_LIKES:
                title = getResources().getString(R.string.choose_likes);
                break;

            default:
                break;
        }

        interestBuilder.setTitle(title);

        final String[] interestArrayList = getStrings(interests);

        interestBuilder.setMultiChoiceItems(interestArrayList, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {
                if (isChecked) {
                    selectedInterests.add(interests.get(position));
                } else {
                    selectedInterests.remove(interests.get(position));
                }
            }
        });

        interestBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                switch (type) {
                    case TYPE_LIKES:
                        likes.addAll(selectedInterests);
                        interests.removeAll(selectedInterests);
                        likesRecyclerView.setAdapter(new ChooceRecyclerViewAdapter(AddCharacterActivity.this, likes, TYPE_LIKES));
                        break;
                    case TYPE_DISLIKES:
                        dislikes.addAll(selectedInterests);
                        interests.removeAll(selectedInterests);
                        dislikesRecyclerView.setAdapter(new ChooceRecyclerViewAdapter(AddCharacterActivity.this, dislikes, TYPE_DISLIKES));
                        break;
                    default:
                        break;
                }
            }
        });

        interestBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        interestBuilder.show();
    }

    public void showLanguagesDialog() {

        final List<Language> tempSelectedLanguages = new ArrayList<>();

        final AlertDialog.Builder interestBuilder = new AlertDialog.Builder(AddCharacterActivity.this);
        String title = getResources().getString(R.string.choose_languages);
        interestBuilder.setTitle(title);

        final String[] interestArrayList = getStringsFromLanguage(languages);

        interestBuilder.setMultiChoiceItems(interestArrayList, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {
                if (isChecked) {
                    tempSelectedLanguages.add(languages.get(position));
                } else {
                    tempSelectedLanguages.remove(languages.get(position));
                }
            }
        });

        interestBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectedLanguages.addAll(tempSelectedLanguages);
                languages.removeAll(tempSelectedLanguages);
                languagesRecyclerView.setAdapter(new ChooceRecyclerViewAdapter(AddCharacterActivity.this, selectedLanguages));
            }
        });

        interestBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        interestBuilder.show();
    }

    public void removeLanguage(Language language) {
        languages.add(language);
        selectedLanguages.remove(language);
        languagesRecyclerView.setAdapter(new ChooceRecyclerViewAdapter(AddCharacterActivity.this, selectedLanguages));
    }

    public void removeLike(int i) {
        Interest temp = likes.get(i);
        interests.add(temp);
        likes.remove(i);
        likesRecyclerView.setAdapter(new ChooceRecyclerViewAdapter(AddCharacterActivity.this, likes, TYPE_LIKES));
    }

    public void removeDislike(int i) {
        Interest temp = dislikes.get(i);
        interests.add(temp);
        dislikes.remove(i);
        dislikesRecyclerView.setAdapter(new ChooceRecyclerViewAdapter(AddCharacterActivity.this, dislikes, TYPE_DISLIKES));
    }

    /**
     * Convert arraylist to string array
     *
     * @param objects
     * @return
     */
    public String[] getStrings(List<Interest> objects) {
        String[] stringArrayList = new String[objects.size()];
        for (Interest object : objects) {
            stringArrayList[objects.indexOf(object)] = object.getName();
        }
        return stringArrayList;
    }

    /**
     * Convert arraylist to string array
     *
     * @param objects
     * @return
     */
    public String[] getStringsFromLanguage(List<Language> objects) {
        String[] stringArrayList = new String[objects.size()];
        for (Language object : objects) {
            stringArrayList[objects.indexOf(object)] = object.getName();
        }
        return stringArrayList;
    }

    public Characteristic getUserCharacteristics() {

        Characteristic characteristic = new Characteristic();

        characteristic.setJob(jobEditText.getText().toString());
        characteristic.setIncome(Double.parseDouble(incomeEditText.getText().toString()));
        characteristic.setDetail(detailEditText.getText().toString());
        characteristic.setReligion(selectedReligion);
        characteristic.setDislikes(dislikes);
        characteristic.setLikes(likes);
        characteristic.setLanguages(selectedLanguages);

        return characteristic;

    }

    public void fetchReligionsFromDB() {
        userDBHandler.getReligions();
    }

    public void fetchLanguagesFromDB() {
        userDBHandler.getLanguages();
    }

    public void fetchInterestsFromDB() {
        userDBHandler.getInterest();
    }

    public void setReligions(List<Religion> _religions) {
        religions = _religions;
        showReligionDialog(false);
    }

    public void setLanguages(List<Language> _languages) {
        languages = _languages;
        showLanguagesDialog();
    }

    public void setInterests(List<Interest> _interests) {
        interests = _interests;
        showInterestsDialog(interest_type);
    }
}


