package com.example.iyoteam.ethioroommate.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.iyoteam.ethioroommate.actors.Room;
import com.example.iyoteam.ethioroommate.R;

import java.util.List;

/**
 * Created by Mikee on 3/26/2018.
 */

public class HouseRecyclerViewAdapter extends RecyclerView.Adapter<HouseRecyclerViewAdapter.ViewHolder> {

    private List<Room> rooms;
    private Context context;

    public HouseRecyclerViewAdapter(List<Room> rooms, Context context){
        this.rooms = rooms;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.house_list_item_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Room room = rooms.get(position);
        holder.titleTextView.setText(room.getCaption());
        holder.locationTextView.setText(room.getLocation());
        holder.priceTextView.setText(room.getPrice().toString() + "Birr");
    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView titleTextView;
        public TextView locationTextView;
        public TextView priceTextView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            titleTextView = itemView.findViewById(R.id.titleTextVIew);
            locationTextView = itemView.findViewById(R.id.locationTextVIew);
            priceTextView = itemView.findViewById(R.id.priceTextView);
        }
    }
}
