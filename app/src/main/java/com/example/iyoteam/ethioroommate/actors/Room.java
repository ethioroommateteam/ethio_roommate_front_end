
package com.example.iyoteam.ethioroommate.actors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Room {

    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("caption")
    @Expose
    private String caption;

    @SerializedName("numberOfPeople")
    @Expose
    private Integer numberOfPeople;

    public Integer getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(Integer numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public Integer getMinAge() {
        return minAge;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    public Integer getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    public Integer getSexType() {
        return sexType;
    }

    public void setSexType(Integer sexType) {
        this.sexType = sexType;
    }

    public Integer getNumberOfRoommates() {
        return numberOfRoommates;
    }

    public void setNumberOfRoommates(Integer numberOfRoommates) {
        this.numberOfRoommates = numberOfRoommates;
    }

    @SerializedName("minAge")
    @Expose
    private Integer minAge;

    @SerializedName("maxAge")
    @Expose
    private Integer maxAge;

    @SerializedName("sexType")
    @Expose
    private Integer sexType;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("latitude")
    @Expose
    private Double latitude;

    @SerializedName("longitude")
    @Expose
    private Double longitude;

    @SerializedName("price")
    @Expose
    private Double price;

    @SerializedName("numberOfRooms")
    @Expose
    private Integer numberOfRooms;

    @SerializedName("numberOfRoommates")
    @Expose
    private Integer numberOfRoommates;

    @SerializedName("description")
    @Expose
    private String description;

    public Room(){};

    public Room(String _caption, String _location, Double _latitude, Double _longitude, Double _price, Integer _number_of_rooms, String _description){
        this.caption = _caption;
        this.location = _location;
        this.latitude = _latitude;
        this.longitude = _longitude;
        this.price = _price;
        this.numberOfRooms = _number_of_rooms;
        this.description = _description;
    }

    public Room(
            String location,
            Double latitude,
            Double longitude,
            Integer numberOfRooms,
            Integer numberOfPeople,
            Integer numberOfRoommates,
            Double price,
            Integer minAge,
            Integer maxAge,
            Integer sexType,
            String caption,
            String description
    ){
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.numberOfRooms = numberOfRooms;
        this.numberOfPeople = numberOfPeople;
        this.numberOfRoommates = numberOfRoommates;
        this. price = price;
        this.minAge = minAge;
        this.maxAge = maxAge;
        this.sexType = sexType;
        this.caption = caption;
        this.description = description;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(Integer numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
