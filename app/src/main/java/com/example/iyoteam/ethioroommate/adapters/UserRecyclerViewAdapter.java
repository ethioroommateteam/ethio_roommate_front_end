package com.example.iyoteam.ethioroommate.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.iyoteam.ethioroommate.activities.UserListActivity;
import com.example.iyoteam.ethioroommate.actors.User;
import com.example.iyoteam.ethioroommate.R;

import java.util.List;

/**
 * Created by Mikee on 3/26/2018.
 */

public class UserRecyclerViewAdapter extends RecyclerView.Adapter<UserRecyclerViewAdapter.ViewHolder> {

    private List<User> users;
    private Context context;

    public UserRecyclerViewAdapter(List<User> users, Context context){
        this.users = users;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_item_for_user_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final User user = users.get(position);
        holder.userListNameTextView.setText(user.getUserName());
        holder.userListJobTextView.setText(user.getPhoneNumber());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((UserListActivity)context).getUserCharacter(user);
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView userListNameTextView;
        public TextView userListJobTextView;
        public CardView userListCardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            userListJobTextView = itemView.findViewById(R.id.userListJobTextView);
            userListNameTextView = itemView.findViewById(R.id.userListNameTextView);
            userListCardView = itemView.findViewById(R.id.userListCardView);

        }
    }

}
