package com.example.iyoteam.ethioroommate.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.iyoteam.ethioroommate.R;

public class MainActivity extends AppCompatActivity {

    private Button signUpBtn;
    private Button signInBtn;

    private EditText userNameEditText;
    private EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        signInBtn = (Button)findViewById(R.id.signInBtn);
        signUpBtn = (Button)findViewById(R.id.signUpBtn);

        userNameEditText = findViewById(R.id.userNameEditText);
        passwordEditText = findViewById(R.id.passwordEditText);

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signUpIntent = new Intent(MainActivity.this, SignUpActivity.class);
                startActivity(signUpIntent);
            }
        });

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String userNameValue = userNameEditText.getText().toString();
                String passwordValue = passwordEditText.getText().toString();

                if( (userNameValue.length() >= 4)&& (passwordValue.length() >= 4)){
                    Intent uploadRentActivity = new Intent(MainActivity.this, UploadRoomActivity.class);
                    startActivity(uploadRentActivity);
                }

                else {
                    Toast.makeText(MainActivity.this, "Please enter the required field", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

}
