package com.example.iyoteam.ethioroommate.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.iyoteam.ethioroommate.R;
import com.example.iyoteam.ethioroommate.adapters.TekerayPageAdapter;

/**
 * Created by Mikee on 3/25/2018.
 */

public class NewTekerayActivity extends AppCompatActivity {

    private TekerayPageAdapter tekerayPageAdapter;

    ViewPager viewPager;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.house_detail);

        tekerayPageAdapter = new TekerayPageAdapter(getSupportFragmentManager(), this);
//        viewPager = findViewById(R.id.viewPager);
//        tabLayout = findViewById(R.id.tabLayout);

//        initializeTab();
    }

//    private void initializeTab() {
//        viewPager.setAdapter(tekerayPageAdapter);
//
//        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//    }
}
